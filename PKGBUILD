# Maintainer: XavierCLL <xavier.corredor.llano (a) gmail.com>

pkgname=insync-dolphin
pkgver=3.8.1.50459
pkgrel=1
pkgdesc="This package contains the service menu for integrating Insync with Dolphin"
url="https://www.insynchq.com/downloads"
license=('custom:insync')
options=('!strip' '!emptydirs')
arch=('x86_64')
makedepends=('imagemagick')
source=("http://cdn.insynchq.com/builds/linux/${pkgname}_${pkgver}_all.deb"
        "https://metainfo.manjariando.com.br/${pkgname}/com.${pkgname}.metainfo.xml"
        "https://metainfo.manjariando.com.br/filemanager.png")
sha256sums=('71f01bf1a2ea00004cf7a916c2a6df6c784460469475b8cb0cc3ea0bef8a088c'
            '326a7c3d2891ea2d410d30119fc73c073bf435479fc222e832204f29d30f1186'
            '3b19be9e841750dd69bc8bedb141dc74a9cac7ec6a885223cc5d1bf26af6d925')
noextract=("${pkgname}_${pkgver}_all.deb")

_insync_desktop="[Desktop Entry]
Version=1.0
Terminal=false
Type=Application
Name=
Exec=insync start
TryExec=insync
Categories=Utility;
NoDisplay=true"

build() {
    cd "${srcdir}"
    echo -e "$_insync_desktop" | tee com.${pkgname/-/_}.desktop
}

package() {
    depends=("insync>=${pkgver%.*}" "dolphin")
    optdepends=("insync-emblem-icons: emblem icons for Insync")

    cd ${srcdir}
    ar x ${pkgname}_${pkgver}_all.deb
    tar xvf data.tar.gz 
    mv usr "${pkgdir}/"
    install -dm 755 "${pkgdir}/usr/lib/qt"
    mv "${pkgdir}/usr/lib/x86_64-linux-gnu/qt5/plugins" "${pkgdir}/usr/lib/qt/"
    echo "Icon=insync" >> "${pkgdir}/usr/share/kservices5/fileitemactioninsyncplugin.desktop"
        
    # Appstream
    install -Dm644 "${srcdir}/com.${pkgname/-/_}.desktop" "${pkgdir}/usr/share/applications/com.${pkgname/-/_}.desktop"
    install -Dm644 "${srcdir}/com.${pkgname}.metainfo.xml" "${pkgdir}/usr/share/metainfo/com.${pkgname}.metainfo.xml"

    for size in 22 24 32 48 64 128; do
        mkdir -p "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps"
        convert "${srcdir}/filemanager.png" -resize "${size}x${size}" \
            "${pkgdir}/usr/share/icons/hicolor/${size}x${size}/apps/${pkgname}.png"
    done
}
